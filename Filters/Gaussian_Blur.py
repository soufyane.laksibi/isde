

from .Filter import Filter
import numpy as np

class GaussianBlur(Filter):
    """
Attributes
     kernel: 3x3 or 5x5 GaussianBlur kernel mask
     kernel_dem: dimension of the kernel mask

Methods
     elect_kernel: configure the GaussianBlur kernel mask
     """
    def elect_kernel(self):


        if self.kernel_dem == 3:


            self._kernel = (1./16) * np.array([[1, 2, 1],
                                               [2, 4, 2],
                                               [1, 2, 1]])

        elif self.kernel_dem == 5:


            self._kernel = (1./256) * np.array([[1, 4, 6, 4, 1],
                                                [4, 16, 24, 16, 4],
                                                [6, 24, 36, 24, 6],
                                                [4, 16, 24, 16, 4],
                                                [1, 4, 6, 4, 1]])

        else:

            raise ValueError("The kernel dimension should be either 3x3 or 5x5")