import numpy as np
from .Filter import Filter


class Sharpen(Filter):


    def elect_kernel(self):
        if self.kernel_dem == 3:
            self._kernel = np.array([[-1, -1, -1],
                                     [-1, 9, -1],
                                     [-1, -1, -1]])
        elif self.kernel_dem == 5:
            kernel = np.array([[-9, -3, -4, -3, -1 ],
                               [-3,  0,  6,  0, -3],
                               [-4,  6, 21,  6, -4 ],
                               [-3,  0,  6,  0, -3],
                               [-1, -3, -4, -3, -1,]])
            k = np.sum(kernel)
            self._kernel = (1./k) * kernel
        else:
            raise ValueError("Try  3x3 or 5x5   ^_^ ")

