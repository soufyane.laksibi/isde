from .Filter import Filter
import numpy as np


class EdgeDetectionVertical(Filter):

    """Class inherited from the class Filter.

Attributes:
kernel: 3x3 or 5x5 Vertical edge detection kernel mask
kernel_dem: dimension of the kernel mask

Methods:
elect_kernel: definition the Vertical edge detection kernel mask
"""
    def elect_kernel(self):

        if self.kernel_dem == 3:
            self._kernel = np.array([[1, 0, -1],
                                     [1, 0, -1],
                                     [1, 0, -1]])
        elif self.kernel_dem == 5:
            self._kernel = np.array([[1, 2, 0, -2, -1],
                                     [4, 8, 0, -8, -4],
                                     [6, 12, 0, -6, -12],
                                     [4, 8, 0, -8, -4],
                                     [1, 2, 0, -2, -1]])
        else:
            raise ValueError("The kernel dimension should be 3x3 or 5x5")

