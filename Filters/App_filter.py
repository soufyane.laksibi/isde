
class AppFilter:
    '''
    class responsible of applying selected filter on a selected image
    '''

    def __init__(self, filter):
        self.filter = filter

    def filter_image(self, image):
        filterd_image = self.filter.appfilter(image)
        return filterd_image




