import numpy as np
from abc import abstractmethod

class Filter:

    """
    Abstract class Filter

    Attributes
    ----------
    kernel: 3x3 or 5x5 kernel mask
    kernel_dem: dimension of the kernel mask

    Methods
    -------
    kernel: getter
    kernel_dem: getter and setter
    elect_kernel: abstract method
    appfilter: applies the specific filter using the convolution function
    FilterConv: performs the convolution between one image and a kernel mask
    """

    def __init__(self, kernel_dem=3):
        self._kernel = None
        self._kernel_dem = None
        self.kernel_dem = kernel_dem


    @property
    def kernel_dem(self):
        return self._kernel_dem

    @property
    def kernel(self):
        return self._kernel

    @kernel_dem.setter
    def kernel_dem(self, value):
        self._kernel_dem = value
        if self.__class__ != Filter:
            self.elect_kernel()

    def FilterConv(self, img, kernel):
        """
          Performs the convolution between the image and the kernel

          Parameters
          ----------
          img: image to be modified
          kernel: kernel mask

          Returns
          -------
          convolution result
          """
        k = self.kernel_dem
        img_padded = np.zeros((img.shape[0] + (k - 1), img.shape[1] + (k - 1)))
        img_padded[int((k - 1) / 2):int(-(k - 1) / 2), int((k - 1) / 2):int(-(k - 1) / 2)] = img
        convolution = np.zeros(img.shape)
        kernel = np.flipud(np.fliplr(kernel))
        for x in range(img.shape[1]):
            for y in range(img.shape[0]):
                convolution[y, x] = (kernel * img_padded[y:y + k, x:x + k]).sum()
        return convolution


    def appfilter(self, img):
        """
        Applies the filter

        Parameters
        ----------
        img: image to be modified

        Returns
        -------
        the filtered image
        """
        img_data = np.array(img, dtype=float)
        img_filtered = np.zeros(shape=img_data.shape)
        if img_data.ndim == 3:
            for i in range(3):
                """for each of the 3 RGB levels apply the convolve operation """
                img_filtered[:, :, i] = self.FilterConv(img_data[:, :, i], self._kernel)
                """ convert the element of the image back to integer"""
            img_filtered = np.array(np.clip(img_filtered, 0, 255), dtype=np.uint8)
        else:
            img_filtered = self.FilterConv(img_data, self._kernel)
            img_filtered = np.array(np.clip(img_filtered, 0, 255), dtype=np.uint8)
        return img_filtered

    @abstractmethod
    def elect_kernel(self):
        """
         Abstract method to be implemented
         """
        raise NotImplementedError("Method not implemented yet")
