from .Filter import Filter
import numpy as np


class BoxBlur(Filter):

    """
 Attributes:
    kernel: 3x3 or 5x5 BoxBlur kernel mask
    kernel_dem: dimension of the kernel mask

Methods:
    elect_kernel: configuration of kernel mask
    """

    def elect_kernel(self):

        if self.kernel_dem == 3:
            self._kernel = (1./9) * np.ones(shape=(3, 3))
        elif self.kernel_dem == 5:
            self._kernel = (1./25) * np.ones(shape=(5, 5))
        else:
            raise ValueError("The kernel dimension should be either 3x3 or 5x5")