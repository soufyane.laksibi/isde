from .Filter import Filter
import numpy as np


class EdgeDetectionHorizontal(Filter):

    """ Class inherited from the class Filter.
Attributes:
kernel: 3x3 or 5x5 Horizontal edge detection kernel mask
kernel_dem: Dimension of the kernel

Methods:
elect_kernel: definition of the Horizontal edge detection kernel
"""
    def elect_kernel(self):

        if self.kernel_dem == 3:
            self._kernel = np.array([[1, 1, 1],
                                     [0, 0, 0],
                                     [-1, -1, -1]])
        elif self.kernel_dem == 5:
            self._kernel = np.array([[1, 4, 6, 4, 1],
                                     [2, 8, 12, 8, 2],
                                     [0, 0, 0, 0, 0],
                                     [-2, -8, -12, -8, -2],
                                     [-1, -4, -6, -4, -1]])

        else:
            raise ValueError("The kernel dimension should be 3x3 or 5x5")
