
class Show:
    """
Show class present the user interface of program

Methods
-------
MainMenu: Main menu is the principal menu of program
load_image: menu of loading the image to be filtered
menu_Select: menu to enter in filtering techniques
Select_filter : menu to choose between  filters
kernel_dimension: menu to select the dimension of  kernel
menu_feltering: Apply filter /save img menu
save_image: message  after saving the filtered image
"""
    def MainMenu(self):

        print("------------ Main menu ------------")
        print("1 : Load an image")
        print("2 : Exit")
        i = input("Input your choice : ")
        return i

    def load_image(self):

        print("----- Load image -----")
        path = input("Enter picture name : ")
        return path

    def menu_Select(self):

        print("------- Filter Technique ------")
        print("1 : Select a filter")
        print("2 : Back to start menu")
        print("3 : Exit")
        i = input("Select your choice :  ")
        return i


    def Select_filter(self):

        print("------ Select Filter ------")
        print("1 : Sharpen")
        print("2 : Blur box")
        print("3 : Blur gaussian")
        print("4 : Horizontal Edge detection")
        print("5 : Vertical Edge detection")
        print("6 : Back to start menu")
        print("7 : Exit")
        i = input(" Your choice : ")
        return i

    def kernel_dimension(self):

        print("----------  Dimension of kernel ----------")
        i = input("Input 3 or 5 : ")
        return i

    def menu_feltering(self):

        print("---------- Filtering  -------------")
        print("1 : Apply function")
        print("2 : Save image")
        print("3 : Back to select filter")
        print("4 : Back to main menu")
        print("5 : Exit ")
        i = input(" Input your choice : ")
        return i

    def save_image(self):

        print("The filtered image was saved (^_^)")