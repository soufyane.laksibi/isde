from .Start_state import StartState

class StatusState():

    """
        Class implements the state design pattern

        Attributes
        ----------
        state: current state (initialized with StartState)

        Methods
        -------
        alter_state: to alter the current state
        menu_show: implements the state
    """

    def __init__(self):
        self.state = StartState()

    def alter_state(self, new_state):
        """
           Method used to alter the current state

           Parameters
           ----------
           new_state: chosen state to be set

           """
        self.state = new_state


    def menu_show(self):
        """
            Method uses the state particular to menu_show method
        """
        self.state.menu_show(self)



