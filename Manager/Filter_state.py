import sys
import matplotlib.pyplot as plt
from .State import State
from Manager import Start_state
from Manager import Select_state


class FilterState(State):
    """
        Inherited from the class State

        Attributes
        ----------
        show: UI to be used (MVC design pattern)
        filter: the received filter from the SelectState
        img: the received image from the SelectState


        Methods
        -------
        menu_show: perform the supplied user input
        plot: modify & plot the current image
        save: save the current image
    """
    def __init__(self, img, filter):
        super().__init__()
        self.img = img
        self.filter = filter
        self.actual_image = img

    def menu_show(self,case):
        """
          Perform the supplied user input

          Parameters
          ----------
          case: to apply the state pattern
        """
        user_in = self.show.menu_feltering()
        if user_in == "1":
            self.plot()
        elif user_in == "2":
            self.save()
            self.show.save_image()
        elif user_in == "3":
            case.alter_state(Select_state.SelectState(self.img))
        elif user_in == "4":
            case.alter_state(Start_state.StartState())
        elif user_in == "5":
            sys.exit()
        else:
            print("Please try again")

    def plot(self):
        """
            Apply the selected filter and plot the current image
        """
        self.actual_image = self.filter.filter_image(self.actual_image)
        plt.subplot(1, 2, 1)
        if self.img.ndim == 3:
            plt.imshow(self.img)
        else:
            plt.imshow(self.img, cmap=plt.cm.gray)
        plt.title("The original image")
        plt.subplot(1, 2, 2)
        if self.actual_image.ndim == 3:
            plt.imshow(self.actual_image)
        else:
            plt.imshow(self.actual_image, cmap=plt.cm.gray)
        plt.title("The filtered image")
        plt.show()

    def save(self):
        """
         save the actual image
         """
        name = "./image/" + input("Save as: ")
        if self.actual_image.ndim == 3:
            plt.imsave(name, self.actual_image)
        else:
            plt.imsave(name , self.actual_image, cmap=plt.cm.gray)










