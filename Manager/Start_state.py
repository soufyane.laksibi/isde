import sys
from Manager.State import State
from Manager.Select_state import SelectState
from imageio import imread


class StartState(State):
    """
         Class StartState inherited from the class Phase


         Methods
         -------
         menu_show: method to hold the user supplied input
     """
 
    def menu_show(self,case):
        """
                Overwritten method used to hold the user supplied input

                Parameters
                ----------
                case: used to apply the case pattern
        """
        user_in = self.show.MainMenu()
        if user_in == "1":
            img_name = self.show.load_image()
            img = imread("./Image/" + img_name)
            case.alter_state(SelectState(img))
        elif user_in == "2":
            sys.exit()
        else:
            print("Please try again: ")










