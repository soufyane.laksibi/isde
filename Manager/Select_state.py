
from Manager.State import State
from Manager import Start_state
from Manager.Filter_state import FilterState
from Filters import AppFilter, Sharpen
import sys

class SelectState(State):


    def __init__(self, img):
        super().__init__()
        self.img = img

    def menu_show(self,case):

        user_input = self.show.menu_Select()
        if user_input == "1":
            filter = {"1": Sharpen}

            input = self.show.Select_filter()
            for key in filter:
                if input == key:
                    kernel_dem = int(self.show.kernel_dimension())
                    filter_3_5 = filter[key](kernel_dem)
                    filtering = AppFilter(filter_3_5)
                    case.alter_state(FilterState(self.img, filtering))
                elif input == "6":
                    case.alter_state(Start_state.StartState())
                elif input == "7":
                    sys.exit()

        elif user_input == "2":
            case.alter_state(Start_state.StartState())
        elif user_input == "3":
            sys.exit()
        else:
            print("Invalid choice")



