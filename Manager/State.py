
from Show import Show
from abc import abstractmethod

class State:
    """
   Abstract class State

   Attributes
   ----------
   show: Menus to be shown to the user as Interface

   Methods
   -------
   show_menu: abstract method to be implemented inside child classes
    """

    def __init__(self):
     self.show = Show()


    @abstractmethod
    def menu_show(self):
        pass
