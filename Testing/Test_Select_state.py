
import unittest
from unittest.mock import patch
from View import Show
from Controller import StartState, SelectState
from Controller.Filter_state import FilterState
from Controller.Status_state import StatusState


class TestSelectState(unittest.TestCase):
    """
    Testing class related to class SelectState

    Methods
    -------
    setUp:
    tearDown:
    test_input_1:
    test_input_2:
    test_input_3:
    """

    def setUp(self):
        """
        set up the  resources : select,case and show object
        from SelectState, StatusState and Show classes
        """
        self.img = '/Image/tree.jpg'
        self.select = SelectState(self.img)
        self.case = StatusState()
        self.show = Show

    def tearDown(self):
        """
        set up the  resources : select,case and show object
        from SelectState, StatusState and Show classes
        """
        del self.select
        del self.case
        del self.show

    def test_input_1(self):
        """
        test the valid  choice
        Returns
        OK if the new state is switched to  FilterState
        """
        with patch('builtins.input', side_effect=["1", "2", "3"]):
            self.select.menu_show(self.case)
            user_input = ['1', '2', '3', '4', '5']
            with patch('builtins.input', side_effect=user_input):
                self.show.Select_filter(self.case)
                self.assertIs(self.case.state.__class__, FilterState)

    def test_input_2(self):
        """
        check the valid user input : 2 switch to start state
    Returns:
        OK if the state is switched to StartState
        """
        with patch('builtins.input', return_value="2"):
            self.select.menu_show(self.case)
            self.assertIs(self.case.state.__class__, StartState)

    def test_input_3(self):
        """
        check the valid user input : 3 switch to start state (Exit from program)
      Returns:
        OK if the program exits
        """
        with patch('builtins.input', return_value="3"):
            with self.assertRaises(SystemExit):
                self.select.menu_show(self.case)


suite = unittest.TestLoader().loadTestsFromTestCase(TestSelectState)
unittest.TextTestRunner(verbosity=2).run(suite)