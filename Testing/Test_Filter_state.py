
import unittest
from unittest.mock import patch
from Controller import StartState, SelectState, FilterState
from Model import Sharpen, Filter
from Controller.Status_state import StatusState
from unittest.mock import MagicMock
from imageio import imread
import os


class TestFilterState(unittest.TestCase):
    """
    Test FilterState class

    Methods:
    setUp:
    tearDown:
    test_input_1:
    test_input_2_action_called:
    test_input_2_image_saved:
    test_input_3:
    test_input_4:
    test_input_5:
    """

    def setUp(self):
        """
        set up of resources : context,sharpen_filter, filter and filtering object
        from StateContext, Sharpen, Filter and FilterState classes
        """
        self.case = StatusState()
        self.img = imread('./Image/tree.jpg')
        self.sharpen = Sharpen()
        self.filter = Filter(self.sharpen)
        self.filtering = FilterState(self.img, self.filter)

    def tearDown(self):
        """
        clean up of resources : context,sharpen_filter, filter and filtering object
        from StateContext, Sharpen, Filter and FilterState classes
        """
        del self.filtering
        del self.filter
        del self.sharpen
        del self.img
        del self.case

    def test_input_1(self):
        """
        test verifies the  behavior of class' FilterState method menu_show - input=1
        return: OK if the method plot function is called
        """
        with patch('builtins.input', side_effect =["1"]):
            self.filtering.plot = MagicMock()
            self.filtering.menu_show(self.case)
            assert self.filtering.plot.called

    def test_input_2(self):
        """
      test verifies the  behavior of class' FilterState method menu_show - input=2
    Returns:
        OK if the method safe function is called
        """
        with patch('builtins.input', side_effect=["2"]):
            self.filtering.save = MagicMock()
            self.filtering.menu_show(self.case)
            assert self.filtering.save.called

    def test_input_2_save(self):
        """
         test verifies the  behavior of class' FilterState method menu_show - input=2 + the user saves an image
    Returns:
        OK if the image is successfully created and saved
        """
        with patch('builtins.input', side_effect=["2", "new_tree.jpg"]):
            self.filtering.menu_show(self.case)
            self.assertTrue(os.path.exists('./Image/new_tree.jpg'))
        os.remove('./Image/new_tree.jpg')

    def test_input3(self):
        """
        test verifies the  behavior of class' FilterState method menu_show - input =3
     Returns:
        OK if the state is switched to SelectState
        """
        with patch('builtins.input', return_value="3"):
            self.filtering.menu_show(self.case)
            self.assertIs(self.case.state.__class__, SelectState)

    def test_input4(self):
        """
       test verifies the  behavior of class' FilterState method menu_show - input =4
    Returns:
        OK if the state is switched to StartState
        """
        with patch('builtins.input', return_value="4"):
            self.filtering.menu_show(self.case)
            self.assertIs(self.case.state.__class__, StartState)

    def test_input5(self):
        """
         test verifies the  behavior of class' FilterState method menu_show - input =5
       Returns:
        OK if the program exits
        """

        with patch('builtins.input', return_value="5"):
            with self.assertRaises(SystemExit):
                self.filtering.menu_show(self.case)


suite = unittest.TestLoader().loadTestsFromTestCase(TestFilterState)
unittest.TextTestRunner(verbosity=2).run(suite)
