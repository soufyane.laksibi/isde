
import unittest
from unittest.mock import patch
from Controller import StartState, SelectState
from Controller.Status_state import StatusState
import io
import sys


class testStart_state(unittest.TestCase):
    """
    Test StartState class
   Methods:
    setUp:
    tearDown:
    test_input_1:
    test_input_2:
    test_input_3:
    """
    def setUp(self):
        """
        set up of the resources : case and start object from StatusState, StartState classes
        """
        self.case = StatusState()
        self.start = StartState()

    def tearDown(self):
        """
        clea up the  resources : case and start object
        from StateContext, StartState classes
        """
        del self.start
        del self.case

    def test_input_1(self):
        """
        test verifies the  behavior of class' StartState  method menu_show - input =1
       Returns:
        OK if the case is switched to SelectState
        """
        with patch('builtins.input', side_effect=["1", "tree.jpg"]):
            self.start.menu_show(self.case)
            self.assertIs(self.case.state.__class__, SelectState)

    def test_input_2(self):
        """
        test verifies the  behavior of class' StartState  method menu_show - input =2
    Returns:
        OK if the program exits from the menu
        """
        with patch('builtins.input', return_value="2"):
            with self.assertRaises(SystemExit):
                self.start.menu_show(self.case)

    def test_input_3(self):
        """
       test verifies class' StartState  method menu_show - input =3
    Returns:
        OK if the string "Invalid choice" is printed
        """
        held = sys.stdout
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        with patch('builtins.input', return_value="9"):
            self.start.menu_show(self.case)
        output = capturedOutput.getvalue().splitlines()
        sys.stdout = held
        last_out = output[-1]
        self.assertEqual(last_out, "Invalid choice")


suite = unittest.TestLoader().loadTestsFromTestCase(testStart_state)
unittest.TextTestRunner(verbosity=2).run(suite)
