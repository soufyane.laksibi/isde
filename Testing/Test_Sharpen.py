
import unittest
from Model.Sharpen import Sharpen
import numpy as np


class TestSharpen(unittest.TestCase):
    """

    Test of Sharpen filter

 Methods :
    setUp:
    tearDown:
    test_kernel_3x3:
    test_kernel_5x5:
    test_kernel_size:

    """
    def setUp(self):
        """
        set up of resources : sharpen object from Sharpen class
        """
        self.sharpen = Sharpen()

    def tearDown(self):
        """
        destroy of resources : sharpen object from Sharpen class
        """
        del self.sharpen

    def test_kernel_3x3(self):
        """
        testing if kernel dimension is 3
     Returns:
        OK if the 3x3 kernel is correct
        """
        np.testing.assert_array_equal(self.sharpen._kernel, np.array([[-1, -1, -1],
                                                                      [-1, 9, -1],
                                                                      [-1, -1, -1]]))

    def test_kernel_5x5(self):
        """
        testing if kernel dimension is 5
    Returns:
        ok if the 5x5 kernel is correct
        """
        self.sharpen.kernel_dem = 5
        x = np.array([[-1, -3, -4, -3, -1],
                      [-3,  0,  6,  0, -3],
                      [-4,  6, 21,  6, -4],
                      [-3,  0,  6,  0, -3],
                      [-1, -3, -4, -3, -1]])
        np.testing.assert_array_equal(self.sharpen._kernel, x)

    def test_incorrect_kernel_dem(self):
        """
        testing the validity of kernel dimension
     Returns:
        ok if a ValueError exception is raised
        """
        with self.assertRaises(ValueError):
            self.sharpen.kernel_dem = 9


suite = unittest.TestLoader().loadTestsFromTestCase(TestSharpen)
unittest.TextTestRunner(verbosity=2).run(suite)
