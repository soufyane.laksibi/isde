
import unittest
from Controller import FilterState, ComboState
from Controller.Status_state import StatusState
from imageio import imread
from unittest.mock import patch


class testComboState(unittest.TestCase):
    """
    Test ComboState class
  Methods:
    setUp:
    tearDown:
    test_valid_number_filters:
    test_invalid_number_filters:
    test_chosen_filters:
    test_input_7:
    """
    def setUp(self):
        """
        set up the resources : case and combo object from StatusState and ComboState classes
        """
        self.case = StatusState()
        self.img = imread('./Image/tree.jpg')
        self.combo = ComboState(self.img)

    def tearDown(self):
        """
        destroy object used inside the test cases
        return:
        clean up the  resources : case and combo object from StatusState and ComboState classes

        """
        del self.case
        del self.img
        del self.combo

    def test_valid_number_filters(self):
        """
        test case about the selected number of filters
        Returns:
        OK if an exception is not raised
        """
        with patch('builtins.input', return_value="3"):
            self.combo.menu_show(self.case)

    def test_invalid_number_filters(self):
        """
        test about number of filters selected (invalid input)
     Returns :
        OK if an exception is raised
        """
        with patch('builtins.input', return_value="a string"):
            with self.assertRaises(ValueError):
                self.combo.menu_show(self.case)

    def test_chosen_filters(self):
        """
        test of class
      Returns:
        OK if switched to FilterState
        """
        with patch('builtins.input', side_effect=["2", "1", "3", "4", "3"]):
            self.combo.menu_show(self.case)
            self.assertIs(self.case.state.__class__, FilterState)

    def test_input_6(self):
        """
        test if the input is 6
      return:
         OK if the system exits
        """
        with patch('builtins.input', side_effect = ["1","6"]):
            with self.assertRaises(SystemExit):
                self.combo.menu_show(self.case)


    def test_input_7(self):
        """
       test if the input is 7
      Returns:
        OK if the program exits
        """
        with patch('builtins.input', side_effect=["1", "7"]):
            with self.assertRaises(SystemExit):
                self.combo.menu_show(self.case)



suite = unittest.TestLoader().loadTestsFromTestCase(testComboState)
unittest.TextTestRunner(verbosity=2).run(suite)
